#ifndef OPNCAP_H
#define OPNCAP_H

typedef int64_t OpncapTime;
static const OpncapTime OpncapTime__never = -999999999;

/* One write packet */
typedef struct OpncapPacket OpncapPacket;
int OpncapPacket__read(OpncapPacket *self, FILE *fp);

/* Single point in time on a transaction or transaction set */
typedef struct OpncapState OpncapState;
/* Initialize an OpncapState to empty with a given time */
void OpncapState__clear(OpncapState *self, OpncapTime newtime);
/* Initialize an OpncapState as a copy of another with a new time */
void OpncapState__clone(OpncapState *self, const OpncapState *src, OpncapTime newtime);

/* Full Opncap state */
typedef struct Opncap Opncap;
/* get the OpncapState containing data for a given time */
OpncapState *Opncap__state_for_time(Opncap *self, OpncapTime time);
/* Get next time for a register's write from a given time */
OpncapTime Opncap__reg_next_write(Opncap *self, uint8_t reg, OpncapTime time);
/* Get contents of a register at a given time */
uint8_t Opncap__reg_get(Opncap *self, uint8_t reg, OpncapTime time);
/* Add a register change to the Opncap's states */
int Opncap__add_regwrite(Opncap *self, uint8_t reg, uint8_t data, OpncapTime time);
/* Cook states */
int Opncap__cook_states(Opncap *self);
/* Read packets for an Opncap file */
int Opncap__read_pkts(Opncap *self, FILE *fp);
/* Read an Opncap file */
int Opncap__read(Opncap *self, FILE *fp);




struct OpncapPacket {
  uint32_t time; /* cycles since last write */
  uint8_t data; /* data written */
  uint8_t addr; /* address written to */
};
struct OpncapState {
  OpncapTime time; /* state time */
  uint8_t regs[0x100]; /* state register map */
  int changes[0x100]; /* state change map (1 for changed, 0 for not) */
};
struct Opncap {
  /* working data */
  OpncapTime curtime;

  /* raw packet information */
  size_t pkt_count; /* number of packets */
  OpncapPacket *pkts; /* packet data */

  /* cooked packet information */
  size_t state_count;
  OpncapState *states;
};

#endif
