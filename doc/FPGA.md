# FPGA Documentation

## Attaching to OPN
The *FPGA* must be connected to the *OPN* via through the *FPGA*'s expansion
header. The VHDL is currently configured to use GPIO 0, the one further from
the edge of the FPGA board. A simple way to attach jumper cables to the actual
chip is to use SMD grabber clips, and clip them to the legs of the *OPN IC*.
The pin mapping is as follows:

     ________  ________ 
    |        \/        |    __________________
    |      YM2203      |   |  FPGA-EXPANSION  |
    |                  |   |                  |
    | 01 GND     D0 40 |   | 01 D0      D1 02 |
    | 02 D1    PHIS 39 |   | 03 D2      D3 04 |
    | 03 D2    PHIM 38 |   | 05 D4      D5 06 |
    | 04 D3      A0 37 |   | 07 D6      D7 08 |
    | 05 D4     /RD 36 |   | 09            10 |
    | 06 D5     /WR 35 |   | 11 ----   GND 12 |
    | 07 D6     /CS 34 |   | 13 A0         14 |
    | 08 D7    IOB7 33 |   | 15            16 |
    | 09 IOA7  IOB6 32 |   | 17            18 |
    | 10 IOA6  IOB5 31 |     19            20 |
    | 11 IOA5  IOB4 30 |     21            22 |
    | 12 IOA4  IOB3 29 |   | 23            24 |
    | 13 IOA3  IOB2 28 |   | 25            26 |
    | 14 IOA2  IOB1 27 |   | 27            28 |
    | 15 IOA1  IOB0 26 |   | 29 ----   GND 30 |
    | 16 IOA0  /IRQ 25 |   | 31            32 |
    | 17 AGND   /IC 24 |   | 33            34 |
    | 18 AO-C  OP-O 23 |   | 35            36 |
    | 19 AO-B    SH 22 |   | 37        /WR 38 |
    | 20 AO-A   Vdd 21 |   | 39 /RD    /CS 40 |
    |__________________|   |__________________|

Only one ground pin needs to be connected to the *OPN*'s ground, either one
will work fine.

## Uploading
Open the `opncap.qpf` project file in *Quartus II*, then start compilation --
easily done by pressing Ctrl+L. Once this is done, go to Tools->Programmer.
Setup the USB-Blaster hardware and upload the compiled bitstream.

## Using
**opncap-fpga** has 3 switches used (SW0-2), which control reset, mode, and
capture type.

***SW0*** controls reset, and should be *LOW* during bitstream upload, then be
switched back to *HIGH*. This may also be switched *LOW* and back to *HIGH*
when you wish to reset **opncap-fpga** for whatever reason.

***SW1*** controls the mode. When *LOW*, **opncap-fpga** is in *capture mode*.
When it is *HIGH*, **opncap-fpga** is in *standby mode*.

***SW2*** controls the registers logged. When *LOW*, only PSG registers will
be logged. When *HIGH*, all registers will be logged. For trojan purposes, this
should probably be kept to *LOW*.

## Capturing
**opncap-fpga**'s capturing is quick and easy. When ***SW0=1,SW1=0***, all
register writes to the *OPN* will be captured according to ***SW2***. After
setting ***SW0*** and ***SW1***, nothing more needs to be done, **opncap-fpga**
will automatically perform all necessary things for capturing data.

The LEDs on the FPGA show the current pointer in the SRAM. By a nice
coincidence, the number of LEDs is the same as the number of address bits on
the SRAM. There is no protection for SRAM address overflow, so in the case that
the capture goes out of control, you should probably switch to *standby mode*
before the address overflows.

## Receiving
**opncap-fpga**'s receiving currently sucks. You need to upload the
`DE1_USB_API.sof` bitstream to the *FPGA*, then use the `DE1_Control_Panel.exe`
program to save the contents of SRAM to a file.
