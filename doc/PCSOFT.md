# PC Software
The PC-side software consists of two parts. There's a library used for parsing
and using the capture files named **opncap-pc**, and a tool to combine capture
files called **reccat**. This document will cover setting up and using them
both.

## opncap-pc
**opncap-pc** is used to create programs that utilize the captured register
activity, such as trojan tools or hardware sound logging for replaying on other
devices.

**opncap-pc**'s functions require no real setup, merely pass in the capture
file and it will break it down into an easier to use form. Documentation on
using the library functions is located at `doc/OPNCAP-PC.md`.

Most programs using **opncap-pc** will use it as a statically linked library,
so to build and install it, use the following commands:

    make
    make install

Depending on your setup, `make install` may need to be run as super-user.

## reccat
**reccat** is a quick and small tool used to combine capture files. Here is the
usage information, taken from the program itself:

    reccat output.cap input1.cap input2.cap [input3.cap [...]]

**reccat** requires at least two captures to combine, but may combine any
number of captures.
