#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>

#include "opncap-pc.h"

/* OpncapPacket implementation */
int OpncapPacket__read(OpncapPacket *self, FILE *fp)
{
  uint8_t tmp[6];
  if(fread(tmp, 2, 3, fp) != 3) {
    perror("read one packet");
    return 1;
  }
  self->time  = tmp[0] << 0;
  self->time |= tmp[1] << 8;
  self->time |= tmp[2] << 16;
  self->time |= tmp[3] << 24;
  self->data  = tmp[4];
  self->addr  = tmp[5];

  return 0;
}

/* OpncapState implementation */
void OpncapState__clear(OpncapState *self, OpncapTime newtime)
{
  size_t i;
  self->time = newtime;
  for(i = 0; i < 0x100; i++) {
    self->regs[i] = 0;
    self->changes[i] = 0;
  }
}
void OpncapState__clone(OpncapState *self, const OpncapState *src, OpncapTime newtime)
{
  size_t i;
  self->time = newtime;
  for(i = 0; i < 0x100; i++) {
    self->regs[i] = src->regs[i];
    self->changes[i] = 0;
  }
}

/* Opncap implementation */
OpncapState *Opncap__state_for_time(Opncap *self, OpncapTime time)
{
  OpncapState *state;
  size_t i;
  /* get the closest state that isn't ahead of us */
  for(i = 0; i < self->state_count; i++) {
    if(self->states[i].time > time) {
      break;
    }
    state = &self->states[i];
  }
  return state;
}
OpncapTime Opncap__reg_next_write(Opncap *self, uint8_t reg, OpncapTime time)
{
  OpncapState *state;
  state = Opncap__state_for_time(self, time);
  for(state++; state->time != OpncapTime__never; state++) {
    if(state->changes[reg])
      break;
  }
  return state->time;
}
uint8_t Opncap__reg_get(Opncap *self, uint8_t reg, OpncapTime time)
{
  return Opncap__state_for_time(self, time)->regs[reg];
}
int Opncap__add_regwrite(Opncap *self, uint8_t reg, uint8_t data, OpncapTime time)
{
  OpncapState *state = NULL;
  size_t i;
  /* if we already have a state for this exact time, let's find it. */
  for(i = 0; i < self->state_count; i++) {
    if(self->states[i].time == time) {
      state = &self->states[i];
      break;
    }
  }
  /* ok we don't have one, create a new state for it then */
  if(state == NULL) {
    self->state_count++;
    self->states = realloc(self->states, sizeof(OpncapState) * (self->state_count+1));
    self->states[self->state_count].time = OpncapTime__never;
    if(self->state_count == 1) { /* if this is the first state, we can't much clone! */
      OpncapState__clear(&self->states[self->state_count-1], time);
    }else{
      OpncapState__clone(&self->states[self->state_count-1], &self->states[self->state_count-2], time);
    }
    state = &self->states[self->state_count-1];
  }
  /* and set the register value */
  state->regs[reg] = data;
  state->changes[reg] = 1;
  return 0;
}
int Opncap__cook_states(Opncap *self)
{
  int ret;
  size_t i;
  uint8_t laddr, ldata;
  for(i = 0; i < self->pkt_count; i++) {
    self->curtime += self->pkts[i].time + 1;
    if(self->pkts[i].time == 0 && /* If it took 1 cycle, and */
       self->pkts[i].addr == laddr && /* if it's the same register, and */
       self->pkts[i].data == ldata) /* if it's the same data... */
      continue; /* Who cares. Don't bother.*/

    if(self->pkts[i].time == 0 && /* If it took 1 cycle, and */
       self->pkts[i].addr == laddr && /* if it's the same register, and */
       self->pkts[i].data != ldata) { /* if last data was different... */
      /* rewrite the last cycle */
      ret = Opncap__add_regwrite(self, self->pkts[i].addr, self->pkts[i].data, self->curtime-1);
    }else{
      /* otherwise just add it. */
      ret = Opncap__add_regwrite(self, self->pkts[i].addr, self->pkts[i].data, self->curtime);
    }
    if(ret)
      return ret;

    laddr = self->pkts[i].addr;
    ldata = self->pkts[i].data;
  }
  return 0;
}
int Opncap__read_pkts(Opncap *self, FILE *fp)
{
  int ret;
  size_t i;
  uint8_t tmp[4];
  if(fread(tmp, 4, 1, fp) != 1) {
    perror("read packet count");
    return 1;
  }
  self->pkt_count  = tmp[0] << 0;
  self->pkt_count |= tmp[1] << 8;
  self->pkt_count |= tmp[2] << 16;
  self->pkt_count |= tmp[3] << 24;

  self->pkts = malloc(sizeof(OpncapPacket) * self->pkt_count);

  for(i = 0; i <= self->pkt_count-1; i++) {
    ret = OpncapPacket__read(&self->pkts[i], fp);
    if(ret) {
      fprintf(stderr, "Error reading a packet\n");
      return 2;
    }
  }

  return 0;
}
int Opncap__read(Opncap *self, FILE *fp)
{
  int ret;

  /* initial read of packet data from file */
  ret = Opncap__read_pkts(self, fp);
  if(ret)
    return ret;

  /* build set of OpncapStates */
  self->curtime = 0;
  self->state_count = 0;
  self->states = malloc(sizeof(OpncapState));
  self->states[0].time = OpncapTime__never;
  ret = Opncap__cook_states(self);
  if(ret)
    return ret;

  return 0;
}
