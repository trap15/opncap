# OPNCAP
***OPNCAP*** is a set of tools to record and parse data logged from a *Yamaha
YM2203* sound chip (here-on, *OPN*). These tools rely on a hardware setup
utilizing an *Altera DE-1 FPGA development board* (here-on, *FPGA*). The
software is all written in fairly simple C, any decent compiler should be able
to make it. I will include compiled binaries when the project is "complete".

## Requirements
The following hardware is required:

  - *Altera DE-1* FPGA development board.
  - Some hardware with a *Yamaha YM2203* (*OPN*) sound chip.

The following software is required:

  - *Altera Quartus II*, for uploading FPGA code.

## Further information
For information on the FPGA-side software, please read `doc/FPGA.md`.

For information on the PC-side software, please read `doc/PCSOFT.md`.

# Links

- <http://daifukkat.su/> - My website
- <https://bitbucket.org/trap15/opncap> - This repository
- <https://bitbucket.org/trap15/nmk004-trojan> - *NMK004* trojan using these tools.

# Greetz
- Charles MacDonald
- austere
- nimitz
- David "Haze" Haywood
- Jonathan "Lord Nightmare" Gevaryahu
- \#raidenii - Forever impossible
- Beer and coffee

# Licensing
All contents within this repository (unless otherwise specified) are licensed under the following terms:

The MIT License (MIT)

Copyright (C)2014 Alex "trap15" Marshall <trap15@raidenii.net>

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
