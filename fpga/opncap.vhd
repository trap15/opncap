------------------------------------------------------------------------------
-- OPNCAP: OPN capture tool for Altera DE1
-- Copyright (C)2014 Alex "trap15" Marshall <trap15@raidenii.net>
------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;

entity opncap is
  port(
    ---- Clocks
    clock_50: in std_logic;
    clock_24: in std_logic_vector(1 downto 0);
    clock_27: in std_logic_vector(1 downto 0);
    ---- GPIO
    gpio_0: in std_logic_vector(35 downto 0);
    gpio_1: in std_logic_vector(35 downto 0);
    ---- SRAM
    sram_addr: out std_logic_vector(17 downto 0);
    sram_dq: inout std_logic_vector(15 downto 0);
    sram_we_n: out std_logic;
    sram_oe_n: out std_logic;
    sram_ub_n: out std_logic;
    sram_lb_n: out std_logic;
    sram_ce_n: out std_logic;
    ---- Simple I/O
    ledr: out std_logic_vector(9 downto 0);
    ledg: out std_logic_vector(7 downto 0);
    hex0: out std_logic_vector(6 downto 0);
    hex1: out std_logic_vector(6 downto 0);
    hex2: out std_logic_vector(6 downto 0);
    hex3: out std_logic_vector(6 downto 0);
    key:  in  std_logic_vector(3 downto 0);
    sw:   in  std_logic_vector(9 downto 0));
end opncap;

architecture arch of opncap is
  -- OPNCAP base
  signal clk_cap: std_logic;
  signal rst_n: std_logic;
  signal curtime: std_logic_vector(31 downto 0);
  signal fsm_pos: std_logic_vector(1 downto 0);
  signal cap_outptr: std_logic_vector(17 downto 0);
  signal is_first: std_logic;
  signal cap_ctr: std_logic_vector(31 downto 0);
  signal cap_limit: natural;
  signal cap_mode: std_logic;

  -- OPN line aliases
  signal opn_dq: std_logic_vector(7 downto 0);
  signal opn_wr_n: std_logic;
  signal opn_rd_n: std_logic;
  signal opn_cs_n: std_logic;
  signal opn_addr: std_logic_vector(0 downto 0);

  -- OPN state
  signal opns_last_reg: std_logic_vector(7 downto 0);
  signal opns_last_val: std_logic_vector(7 downto 0);
  signal opns_latch_reg: std_logic;
  signal opns_latch_val: std_logic_vector(7 downto 0);
  signal opns_datardy: std_logic;
  signal opns_datardy_last: std_logic;
begin
  -- OPN line aliases
  opn_dq <= gpio_0(7 downto 0);
  opn_wr_n <= gpio_0(33);
  opn_rd_n <= gpio_0(34);
  opn_cs_n <= gpio_0(35);
  opn_addr <= gpio_0(10 downto 10);

  -- Status display
  ledg(7 downto 0) <= cap_outptr(7 downto 0);
  ledr(9 downto 0) <= cap_outptr(17 downto 8);

  hex0 <= opn_dq(6 downto 0);
  hex1(0) <= opn_dq(7);
  hex1(6 downto 1) <= (others => '1');

  hex2 <= curtime(6 downto 0);
  hex3(0) <= opn_wr_n;
  hex3(1) <= opn_cs_n;
  hex3(2) <= opn_rd_n;
  hex3(3) <= opn_addr(0);
  hex3(6 downto 4) <= (others => '1');

  -- OPNCAP update
  rst_n <= sw(0);
  cap_mode <= sw(1);
  clk_cap <= clock_24(0);
  process(sw(2))
  begin
    if(sw(2) = '0') then
      cap_limit <= 16;
    else
      cap_limit <= 256;
    end if;
  end process;
  process(clk_cap, rst_n, opn_cs_n, opn_wr_n, opn_rd_n, opn_dq)
  begin
    if(rising_edge(clk_cap)) then
      if(rst_n = '0') then -- Reset
        opns_datardy <= '0';
        opns_datardy_last <= '0';
        opns_last_reg <= "00000000";
        curtime <= (others => '0');
        fsm_pos <= "00";
        cap_outptr <= "000000000000000010";
        is_first <= '1';
        cap_ctr <= (others => '0');

        sram_lb_n <= '0';
        sram_ub_n <= '0';
        sram_ce_n <= '0';
        sram_oe_n <= '1';
        sram_we_n <= '1';
      elsif(cap_mode = '1') then -- Data upload mode
        -- TODO
      elsif(cap_mode = '0') then -- OPN capture mode
        case fsm_pos is
          when "00" => State0: -- Write current time low
            if(opns_datardy = '1') then
              sram_addr <= unsigned(cap_outptr) + 0;
              sram_we_n <= '0';
              sram_dq <= curtime(15 downto 0);
            else -- Write captured packet count low
              sram_addr <= (others => '0');
              sram_we_n <= '0';
              sram_dq <= cap_ctr(15 downto 0);
            end if;
            fsm_pos <= "01";
          when "01" => -- State1: Write current time high
            if(opns_datardy = '1') then
              sram_addr <= unsigned(cap_outptr) + 1;
              sram_we_n <= '0';
              sram_dq <= curtime(31 downto 16);
            else -- Write captured packet count high
              sram_addr <= "000000000000000001";
              sram_we_n <= '0';
              sram_dq <= cap_ctr(31 downto 16);
            end if;
            fsm_pos <= "10";
          when "10" => -- State2: Write last address+value
            opns_datardy_last <= opns_datardy;
            if(opns_datardy = '1') then
              sram_addr <= unsigned(cap_outptr) + 2;
              sram_we_n <= '0';
              sram_dq <= opns_last_reg & opns_last_val;
            else
              sram_we_n <= '1';
            end if;
            fsm_pos <= "11";
          when others => -- State3: Capture
            sram_we_n <= '1';

            if(opns_datardy_last = '1') then
              opns_datardy_last <= '0';
              cap_outptr <= unsigned(cap_outptr) + 3;
              cap_ctr <= unsigned(cap_ctr) + 1;
              curtime <= (others => '0');
              is_first <= '0';
            elsif(is_first = '0') then -- First entry should always be 0 time.
              curtime <= curtime + "00000000000000000000000000000001";
            end if;

            if(opn_wr_n = '0' and opn_cs_n = '0') then -- Something was being written
              if(opn_addr(0) = '0') then -- Register address write
                opns_datardy <= '0';
                opns_last_reg <= opn_dq;
                opns_latch_reg <= '1';
              else -- Register data write
                if(unsigned(opns_last_reg) < cap_limit) then
                  opns_last_val <= opn_dq;
                  if(opns_last_val /= opns_latch_val or opns_latch_reg = '1') then
                    opns_latch_reg <= '0';
                    opns_latch_val <= opns_last_val;

                    opns_datardy <= '1';
                  end if;
                end if;
              end if;
            else
              opns_datardy <= '0';
            end if;

            fsm_pos <= "00";
        end case;
      end if;
    end if;
  end process;  
end arch;
