#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <string.h>

void usage(const char *app)
{
  fprintf(stderr, "Usage:\n\t"
    "%s output.cap input1.cap input2.cap [input3.cap [...]]\n",
    app);
}

int main(int argc, char *argv[])
{
  int count, i;
  char **srcfns, *dstfn;
  uint32_t reccnt;
  uint8_t tmp[4];
  FILE **srcfps, *dstfp;

  if(argc < 4) {
    usage(argv[0]);
    return EXIT_FAILURE;
  }

  reccnt = 0;
  count = argc - 2;
  dstfn = argv[1];
  srcfns = malloc(sizeof(char*) * count);
  srcfps = malloc(sizeof(FILE*) * count);
  for(i = 0; i < count; i++) {
    srcfns[i] = argv[2+i];
  }

  dstfp = fopen(dstfn, "wb");
  for(i = 0; i < count; i++) {
    srcfps[i] = fopen(srcfns[i], "rb");
    fread(tmp, 4, 1, srcfps[i]);
    reccnt += (tmp[0] << 0) | (tmp[1] << 8) | (tmp[2] << 16) | (tmp[3] << 24);
  }

  tmp[0] = reccnt >> 0;
  tmp[1] = reccnt >> 8;
  tmp[2] = reccnt >> 16;
  tmp[3] = reccnt >> 24;
  fwrite(tmp, 4, 1, dstfp);

  for(i = 0; i < count; i++) {
    while(!feof(srcfps[i])) {
      if(fread(tmp, 1, 1, srcfps[i]) != 1)
        break;
      fwrite(tmp, 1, 1, dstfp);
    }
    fclose(srcfps[i]);
  }

  fclose(dstfp);
  free(srcfps);
  free(srcfns);

  return EXIT_SUCCESS;
}
